class NameForm extends React.Component {
  constructor(props) {
    super(props);    
    // bind onSubmit and onInput
    this.onSubmit = this.onSubmit.bind(this);
    this.onInputUserName = this.onInputUserName.bind(this);
	
	
	// init state
    this.state = {
	  inputUserName1: '',
	  inputUserName2: ''
    };
  }
  
  // input change handler
  onInputUserName(e) {
    this.setState({
	  [e.target.name]: e.target.value 
    });
  }

  
  // submit handler
  onSubmit(e) {
    this.props.onSubmit(this.state);
	e.preventDefault();
  }
	
  render() {
    return (
    <div>
      <form onSubmit={this.onSubmit}>
	    <h3 className="header">Please enter the users name to start the game</h3>
        <div>
			<label>
			  User Name 1:
			</label>
			<span className="badge" />
			<input type="text" name="inputUserName1" value={this.state.value} onChange={this.onInputUserName} />
		</div>
		<div>
			<label>
			  User Name 2:
			 </label>
			 <span className="badge" />
			 <input type="text" name="inputUserName2" value={this.state.value} onChange={this.onInputUserName} />
			
		</div>
		<div>
			<input type="submit" value="Submit" />
		</div>
      </form>
     </div>
    );
  }
  
  
}

function Square(props) {
  return (
 <button className= {props.value == 'X' ? 'square x-character' : 'square y-character'} onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        value={this.props.squares[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    return (
      <div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component {
  constructor() {
    super();
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,
      userName1: '',
	  userName2: ''
    };
	
	this.addTodo = this.addTodo.bind(this);
  }

  addTodo (inputUser){
	const userName1 = inputUser.inputUserName1;
	const userName2 = inputUser.inputUserName2;
  this.setState({ userName1 : userName1,
					userName2 : userName2 });
  }
  
  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: history.concat([
        {
          squares: squares
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  resetGameFnc() {
    this.setState({
			stepNumber : 0
	});
  }
  
  resetUser() {
	  this.setState({
			userName1 : '',
			userName2 : ''
		});
  }
  
  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);
    const userName1 = this.state.userName1;
	const userName2 = this.state.userName2;
	const resetGame  = this.state.history.slice(0, 1);
	
    let status;
    if (winner) {
      status = "Winner: " + (this.state.stepNumber%2!=0 ? userName1 : userName2);
    } else if(this.state.stepNumber === 9 ) {
	  status = "Draw";
	} else if (userName1 && userName2) {
      status = (this.state.xIsNext ? userName1 : userName2) + "'s turn";
    }

    return (
    <div className="game">
	{   
	  (!userName1 || !userName2) &&
		<NameForm onSubmit={this.addTodo}/>
	}
       
	{ (userName1 && userName2) &&
        <div className="game-board">
          <Board
            squares={current.squares}
            onClick={i => this.handleClick(i)}
          />
		  <ol className ="btn-area">
			<button onClick={() => this.resetGameFnc()}>Reset game</button>
			<span className="badge"/>
			<button onClick={() => this.resetUser()}>New player</button>
		  </ol>
		  
        </div>
		
	}  
		<div className="game-info">{status}</div>
	
    </div>
	
    );
  }
}

// ========================================

ReactDOM.render(<Game />, document.getElementById("root"));

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}